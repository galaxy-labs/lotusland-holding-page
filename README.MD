
# Guide #

## Dependencies
`npm` 4.5.0
`gulp` 3.9.1

## Development Init
`npm install`

## Process
`gulp` manually compiles files into `dist` folder

`gulp watch` starts watch

## Settings
Rename the file `config.json.sample` to `config.json`

### PHP
If you need a php environment during development or you prefer to keep the source and public files separately, you should:
- configure the `"developers"` option. There are examples for platforms for PC and Mac
- Use the `gulp` tasks with `--to {path}` flag as follows: `gulp --to pc` or `gulp watch --to pc`

### FTP
`"upload"` option provides remote control of the server via ftp. Note that `config.json` will be ignored by git for security reasons
ATTENTION: the new files will be downloaded, and the old ones will be overwritten, but if you need to delete something, you can do this only manually using the ftp manager.

If the development is on a wordpress platform, set `wordpress` option to `true`. This will load the source files from the `wp-theme`.
By default, it will use the `html` folder from the `app`

### Production mode
Production mode `"production": true` provides the ability to uglify/minify .js and .css, as well as compress images
