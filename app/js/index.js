import requestAnimationFrame from 'request-animation-frame-polyfill';
import {navigationEvents, navIsOpen} from './components/navigation';

$(document).ready(function () {
	jQuery.noConflict();
	// A P P L I C A T I O N     C O D E
	navigationEvents();
	console.log(navIsOpen);
});

export function scrollTop() {
	if(typeof pageYOffset != 'undefined'){
		//most browsers except IE before #9
		return pageYOffset;
	}
	else{
		var B = document.body; //IE 'quirks'
		var D = document.documentElement; //IE with doctype
		D = (D.clientHeight) ? D : B;
		return D.scrollTop;
	}
}
export var isIe = (navigator.appName == 'Microsoft Internet Explorer' || (navigator.appName == "Netscape" && navigator.appVersion.indexOf('Edge') > -1));
export var isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));