// Gulp plugins:

var gulp = 				require('gulp'),
	sass = 				require('gulp-sass'),
	sourcemaps = 		require('gulp-sourcemaps'),
	cssnano = 			require('gulp-cssnano'),
	imagemin = 			require('gulp-imagemin'),
	sftp = 				require('gulp-sftp'),
	newer = 			require('gulp-newer'),	
	gulpif = 			require('gulp-if'),
	plumber = 			require('gulp-plumber'),
	webpack =			require('webpack-stream'),
	normalize =			require('node-normalize-scss').includePaths,
	fs = 				require('fs'),
	args = 				require('yargs').argv,
	wbp = 				require('webpack'),
	webpackConfig = 	require("./webpack.config.js")

// Path variables:

var paths = { src:	'app/',	 dist:	'dist/' },
	src = {
		html: 	paths.src + 'html/**/*',
		wp:		paths.src + 'wp-theme/**/*',
		js: 	paths.src + 'js/**/*.js',
		sass:	paths.src + 'sass/**/*.+(scss|sass|less)',
		media: 	paths.src + 'media/**/*',
		fonts: 	paths.src + 'fonts/**/*'
	},
	webpackConfig = require("./webpack.config.js"),
	config = JSON.parse(fs.readFileSync('config.json')),
	sftpConfig 	= {
		host: config.upload.host,
		user: config.upload.log,
		pass: config.upload.pass,
		remotePath: config.upload.dest
	}

if( args.to in config.developers ) {
	paths.dist = config.developers[args.to];
}
var upload = (args.to == "upload") ? true : false,
	dist = {
		css:	paths.dist + 'css',
		fonts: 	paths.dist + 'fonts',
		html:	paths.dist,
		media: 	paths.dist + 'media',
		js:		paths.dist + 'js'
	}
gulp.task('default', ['html', 'sass', 'media', 'fonts', 'webpack', ], function(callback) {});
gulp.task('watch', ['sass:watch', 'html:watch', 'webpack:watch'], function() {}); 

gulp.task('webpack', function (done) {
	return gulp.src(src.js)
		.pipe( webpack(webpackConfig, wbp) )
		.pipe( gulp.dest(dist.js) )
		.pipe( gulpif(upload, sftp(getRemote('js'))) )
});


gulp.task('sass', function () {
	return gulp.src(src.sass)
		.pipe(gulpif( !config.production, sourcemaps.init() ))
		.pipe(plumber())
		.pipe(sass.sync({ includePaths: [].concat(normalize) }))
		.pipe(plumber.stop())
		.pipe(gulpif( config.production, cssnano() ))
		.pipe(gulpif( !config.production, sourcemaps.write() ))
		.pipe(gulp.dest(dist.css))
		.pipe(gulpif(upload, sftp(getRemote('css'))))
});
gulp.task('html', function () {
	var input = config.wordpress ? src.wp : src.html;
	return gulp.src([input])
		.pipe(newer(dist.html))
		.pipe(gulp.dest(dist.html))
		.pipe(gulpif(upload, sftp(getRemote('')))) 
});
gulp.task('media', function () {
	return gulp.src(src.media)
		.pipe(newer(dist.media))
		.pipe(gulpif( config.production, imagemin() ))
		.pipe(gulp.dest(dist.media))
		.pipe(gulpif(upload, sftp(getRemote('media'))))
})
gulp.task('fonts', function () {
	return gulp.src([src.fonts])
		.pipe(newer(dist.fonts))
		.pipe(gulp.dest(dist.fonts))
		.pipe(gulpif(upload, sftp(getRemote('fonts'))))
});
gulp.task('webpack:watch', function (done) {
	var myConfig = Object.create(webpackConfig);
	myConfig.watch = true;
	return gulp.src(src.js)
		.pipe( webpack(myConfig, wbp) )
		.pipe( gulp.dest(dist.js) )
		.pipe(gulpif(upload, sftp(getRemote('js'))))
});  
gulp.task('sass:watch', function () {
	gulp.watch(src.sass, ['sass']).on('change', watchLog);
})
gulp.task('html:watch', function () {
	gulp.watch([src.html, src.wp], ['html']).on('change', watchLog);
})
gulp.task('media:watch', function () {
	gulp.watch(src.media, ['media']).on('change', watchLog);
})
gulp.task('fonts:watch', function () {
	gulp.watch(src.fonts, ['fonts']).on('change', watchLog);
})

function watchLog(event) {
	console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
}

function getRemote(dir) {
	var output = {
		host: config.upload.host,
		user: config.upload.log,
		pass: config.upload.pass,
		remotePath: config.upload.dest + dir
	}
	return output;
}
